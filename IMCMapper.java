import java.io.IOException;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.util.StringTokenizer;



public class IMCMapper extends Mapper<Object, Text, Text, DoubleWritable> {
  
    private DoubleWritable result = new DoubleWritable();
    private Text gender = new Text();
  
    public void map(Object key, Text value, Context context) 
      throws IOException, InterruptedException {
      
        
      String[] fields = value.toString().split(",");
      if (!fields[1].equals("age")) {
      String sexe = fields[4];
      double weight = Double.parseDouble(fields[2]);
      double height = Double.parseDouble(fields[3]);
      
      double bmi = weight / (height * height);
      result.set(bmi);
  
      if (sexe.equals("Female")) {
        gender.set("Femmes");
      } else {
        gender.set("Hommes");
      }
      
      context.write(gender, result);
    }
  }
}
  