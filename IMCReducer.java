import java.io.IOException;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class IMCReducer extends Reducer<Text, DoubleWritable, Text, Text> {

    private double sum = 0.0;
    private double sumOfSquares = 0.0;
    private double count = 0.0;
    private double min = 0.0;
    private double max = 0.0;

    public void reduce(Text key, Iterable<DoubleWritable> values, Context context)
            throws IOException, InterruptedException {

        for (DoubleWritable value : values) {
            double weight = value.get();
            sum += weight;
            sumOfSquares += weight * weight;
            count += 1;
            if (weight < min) {
                min = weight;
            }
            if (weight > max) {
                max = weight;
            }
        }

        double mean = sum / count;
        double variance = (sumOfSquares / count) - (mean * mean);
        double standardDeviation = Math.sqrt(variance);
        String result = String.format("Mean = %.2f, Ecart-type = %.2f, min = %.2f, max = %.2f",
            mean, standardDeviation, min, max);
    context.write(key, new Text(result));
    }


}
